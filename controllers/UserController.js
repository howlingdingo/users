
const express = require('express');
const router = express.Router();
const logger = require('../logger')

/* list all users */
exports.list = (_req, res, _next) => {
    logger.info('getting users')
    User.findAll()
    .then((users) => {
       return res.json(users);
    })
}