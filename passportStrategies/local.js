/**
 * Module dependencies
 */
const passport = require('passport');

const logger = require('../logger');
const User = require('../models/User');
const LocalStrategy = require('passport-local').Strategy;
logger.info('--> loading strategy - local')

module.exports = function localPassport() {
  // Use local strategy
  passport.use(
    new LocalStrategy(
      { usernameField: 'email' },
      (email, password, done) => {
      logger.info(`Inside login basic strategy: ${email}, ${password}`)
      User.findOne({
        where: {
          email: email.toLowerCase(),
        },
      })
        .then((user) => {
          if (user) {
            logger.info(`Inside login basic strategy found user!`)
          } else {
            logger.info(`no user `)
          }

          if (!user || !user.authenticate(password)) {
            return done({
              unauthorised: true,  
              message: `Invalid username or password (${new Date().toLocaleTimeString()})`
            }, null, null);
          }
          return done(null, user);
        })
        .catch((err) => done(err));
    }),
  );
};
