const express = require('express');
const router = express.Router();
const passport = require('passport');

const logger = require('../logger')
module.exports = router;

router.post('/login', (req, res, next) => {
    logger.info('Login - Using passport authenticate');
    const passportCallback = (error, user, info) => {
        if (info) {
            return res.send(info.message)
        };
        if (error) { 
            if (error.unauthorised === true) {
                return res.status(403).json({message: 'bad login'})
            }
            return next(error);
        }
        if (!user) { 
            return res.status(403).json({message: 'bad login'});
        }
        req.login(user, (error) => {
            if (error) {
                logger.error({message: 'error logging in', error})
                return res.status(403).json({message: 'error logging in'});
            }
            return res.json(user);
      })
    }
    passport.authenticate('local', passportCallback)(req, res, next)
})

router.post('/logout', (req, res, _next) => {
    req.logout();
    res.json({message: 'logout success'});
})