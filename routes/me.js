const express = require('express');
const router = express.Router();

router.get('/', function(req, res, _next) {
    if (req.isAuthenticated()) {
        return res.json(req.user);
    }
    return res.status(403).json({message: 'Please Authenticate'})
});

module.exports = router;
