const express = require('express');
const router = express.Router();

module.exports = router;

router.get('/status', function(req, res, _next) {
    // some logic here to validate that we can get to the db???
    res.json({status: 'up'});    
});

module.exports = router;
